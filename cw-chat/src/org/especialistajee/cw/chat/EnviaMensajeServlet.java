package org.especialistajee.cw.chat;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.especialistajee.cw.chat.to.ColaMensajes;
import org.especialistajee.cw.chat.to.Mensaje;

@WebServlet("/EnviaMensajeServlet")
public class EnviaMensajeServlet extends HttpServlet {

	private static final long serialVersionUID = -481257371131179080L;
	private ServletContext ctx;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession();
		this.ctx = req.getServletContext();
		String nick = (String) session.getAttribute("org.especialistajee.cw.chat.nick");
		// TODO: Si no hay usuario, redireccionar a /chat/error.html
		if (nick.isEmpty() || nick == null) {
			RequestDispatcher rd = this.ctx.getRequestDispatcher("/chat/error.html");
			rd.forward(req, res);
			return;
		} else {
			// TODO: Agregar mensaje enviado a la cola de mensajes
			ColaMensajes cm = (ColaMensajes) ctx.getAttribute("cola");
			Mensaje m = new Mensaje(nick, req.getParameter("texto"));
			cm.addMessage(m);
			//this.ctx.setAttribute("cola", cm);
			// TODO: Redireccionar a /chat/enviaMensaje.html
			RequestDispatcher rd = this.ctx.getRequestDispatcher("/chat/enviaMensaje.html");
			rd.include(req, res);
		}
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}
}
