package org.especialistajee.cw.chat;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.especialistajee.cw.chat.to.ColaMensajes;
import org.especialistajee.cw.chat.to.Mensaje;

@WebServlet("/ListaMensajesServlet")
public class ListaMensajesServlet extends HttpServlet {

	private static final long serialVersionUID = 427199619647569137L;

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// TODO: Añadir cabecera para actualizar cada 5 segundos
		res.setIntHeader("Refresh", 5);
		res.setContentType("text/html");
		// TODO: Incluir /chat/cabecera.htmlf
		PrintWriter out = res.getWriter();
		RequestDispatcher rd = getServletContext().getRequestDispatcher("/chat/cabecera.htmlf");
		rd.include(req, res);
		// TODO: Mostrar mensajes del chat
		for (Mensaje m : (ColaMensajes) req.getServletContext().getAttribute("cola")) {
			out.write(m.getEmisor() + " -> " + m.getTexto() + "</br>");
		}
		// TODO: Incluir /chat/pie.htmlf
		rd = getServletContext().getRequestDispatcher("/chat/pie.htmlf");
		rd.include(req, res);
		out.close();
	}

	public void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		doGet(req, res);
	}
}
